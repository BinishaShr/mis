
import DataTypes.Members;
import DatabaseHandling.MemberFetch;
import java.io.FileInputStream;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author binisha
 */
public class MemberDetails {
    int id;
    public MemberDetails(int id){
        this.id=id;
    }
    public Scene getScene(Stage primaryStage) throws Exception{
		
		//--sidebar
		
		VBox navbar=new VBox();
		navbar.setPadding(new Insets(10, 10, 10, 10));
//		navbar.setMinWidth(30);
		navbar.setMinHeight(1000);
		navbar.setBorder(new Border(new BorderStroke(Color.CADETBLUE, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		navbar.setSpacing(20);
		
		//navbar items
		
		//for image icon
		FileInputStream input=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");  
                Image image = new Image(input);  
                ImageView img=new ImageView(image);
                Button icon=new Button("",img);
                icon.setWrapText(true);
		img.setFitHeight(275);
		img.setFitWidth(258);
		icon.setDisable(true);
		icon.setPadding(new Insets(20,20,20,20));
		navbar.setMargin(icon, new Insets(0, 0, 50, 0));
	    
		Button home=new Button("Home");
		home.setMinWidth(300);
		home.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MainPage mainpage=new MainPage();
				try {
					Scene scene= mainpage.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					primaryStage.setMaximized(true);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button projects=new Button("All projects");
		projects.setMinWidth(300);
		projects.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				ProjectList projectlist=new ProjectList();
				try {
					Scene scene= projectlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button members=new Button("Project members");
		members.setMinWidth(300);
		members.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MemberList memberlist=new MemberList();
				try {
					Scene scene= memberlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button settings=new Button("Settings");
		settings.setMinWidth(300);
		
		navbar.getChildren().addAll(icon, home, projects, members, settings);
		
		//--sidebar
                MemberFetch m=new MemberFetch();
                Members member=new Members();
                member=m.getMembers(id);
                VBox details=new VBox();
                Text name=new Text(member.name);
                Text role=new Text(" ( "+member.role+" ) ");
                Text projectsinvolved=new Text("Khali");
                details.getChildren().addAll(name,role,projectsinvolved);
                
                GridPane main=new GridPane();
		main.addRow(0, navbar, details);
		

		Scene scene= new Scene(main,1960,990);
		return(scene);
        }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabaseHandling;

import DataTypes.Members;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import java.sql.ResultSet;

/**
 *
 * @author binisha
 */
public class MemberFetch {
    Connection con;
    Statement stmt;
    ResultSet rs;
    public void connection(){
        try{
            	Class.forName("com.mysql.jdbc.Driver");  
                	  
        }
        catch(ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null,"Unable to register class "+e.getMessage());
        }

        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/project","root","");
            stmt=con.createStatement();  
        }catch (SQLException e){
            JOptionPane.showMessageDialog(null,"Unable to connect to database "+e.getMessage());
        }
    }
    public int getCount(){
        connection();
        int count=0;
        String sql="SELECT COUNT(member_id) AS number From members;";
        try{
            rs=stmt.executeQuery(sql);
            while(rs.next()) { 
                count = rs.getInt("number"); 
            }
            con.close();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"SQL query milena bro."+e.getMessage());
        }
        return count;
    }
    public Members[] getMembers(){
        Members members[]=new Members[100];
        connection();
        int i=0;
//        String name[]=new String[50];
        String sql="SELECT * FROM `members`;";
        try{
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                int id=rs.getInt(1);
                String name = rs.getString(2);
                String role=rs.getString(3);
                members[i]=new Members(id,name,role);
                i++;
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"SQL display query milena bro."+e.getMessage());
        }
//        return members;
        return members;
    }
    
    public Members getMembers(int id){
        Members member=new Members();
        connection();
        int i=0;
//        String name[]=new String[50];
        String sql="SELECT member_name,role FROM `members` WHERE member_id="+id+";";
        try{
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                String name = rs.getString(1);
                String role=rs.getString(2);
                member=new Members(id,name,role);
                i++;
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"SQL display query milena bro."+e.getMessage());
        }
//        return members;
        return member;
    }
//    public static void main(String args[]){
//        MemberFetch m=new MemberFetch();
//        Members a[]=m.getMembers();
//        int count=m.getCount();
//        for(int i=0;i<count;i++)
//        {
//            System.out.println(a[i].name);
//        }
//    }
}

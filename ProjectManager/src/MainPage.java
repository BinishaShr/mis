import java.io.FileInputStream;

import javax.swing.BorderFactory;

import javafx.application.Application;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.Stage;
 
public class MainPage{
	
	public Scene getScene(Stage primaryStage) throws Exception {
		
		//--sidebar
		
		VBox navbar=new VBox();
		navbar.setPadding(new Insets(10, 10, 10, 10));
//		navbar.setMinWidth(30);
		navbar.setMinHeight(1000);
		navbar.setBorder(new Border(new BorderStroke(Color.CADETBLUE, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		navbar.setSpacing(20);
		
		//navbar items
		
		//for image icon
		FileInputStream input=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");  
	    Image image = new Image(input);  
	    ImageView img=new ImageView(image);
	    Button icon=new Button("",img);
	    icon.setWrapText(true);
		img.setFitHeight(275);
		img.setFitWidth(258);
		icon.setDisable(true);
		icon.setPadding(new Insets(20,20,20,20));
		navbar.setMargin(icon, new Insets(0, 0, 50, 0));
	    
		Button home=new Button("Home");
		home.setMinWidth(300);
		home.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MainPage mainpage=new MainPage();
				try {
					Scene scene= mainpage.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					primaryStage.setMaximized(true);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button projects=new Button("All projects");
		projects.setMinWidth(300);
		projects.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				ProjectList projectlist=new ProjectList();
				try {
					Scene scene= projectlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button members=new Button("Project members");
		members.setMinWidth(300);
		members.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MemberList memberlist=new MemberList();
				try {
					Scene scene= memberlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button settings=new Button("Settings");
		settings.setMinWidth(300);
		
		navbar.getChildren().addAll(icon, home, projects, members, settings);
		
		//--sidebar
		
		
		//--projects list
		
		FlowPane mainWin=new FlowPane();
		mainWin.setMinWidth(1595);
		for(int i=0; i<=5; i++) {
	        
	        Text text = new Text("Project name");
	        text.setTextAlignment(TextAlignment.CENTER);
			FileInputStream inputM=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\file_icon.png");
			Image imageM=new Image(inputM);
			ImageView imgM=new ImageView(imageM);
			Label a_icon=new Label("",imgM);
			a_icon.setWrapText(true);
			imgM.setFitHeight(275);
			imgM.setFitWidth(230);
			a_icon.setPadding(new Insets(0,0,0,0));
			VBox vbox=new VBox();
			vbox.setMargin(text, new Insets(0,0,0,60));
			vbox.getChildren().addAll(a_icon, text);
			mainWin.getChildren().add(vbox);
	        mainWin.setMargin(vbox, new Insets(50, 30, 50, 30));
		}
		FileInputStream add_img=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\add.png");
		Image add_image=new Image(add_img);
		ImageView a_img=new ImageView(add_image);
		Label a_icon=new Label("",a_img);
		a_icon.setWrapText(true);
		a_img.setFitHeight(200);
		a_img.setFitWidth(200);
		a_icon.setPadding(new Insets(0,0,0,0));
//		a_icon.setBlendMode(BlendMode.SRC_OVER);
		a_icon.setOnMouseClicked(new EventHandler<MouseEvent>() {
			
			public void handle(MouseEvent event) {
				AddProject addproject=new AddProject();
				try {
					Scene scene= addproject.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		mainWin.getChildren().add(a_icon);
		mainWin.setMargin(a_icon, new Insets(50,50,50,50));
		
		ScrollPane mainscrollPane = new ScrollPane(mainWin);
		mainscrollPane.setMaxHeight(650);
		mainscrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
//	    mainscrollPane.setFitToHeight(true);
		//--projects list
		
		
		//--Recently completed projects
		
		VBox bot=new VBox();
		Text completed=new Text("Recently completed");
		completed.setFont(Font.font("Abyssinica SIL",FontWeight.SEMI_BOLD,FontPosture.REGULAR,20));
		FlowPane recent=new FlowPane();
		recent.setMinWidth(1595);
		
		for(int i=0; i<=4; i++) {
			FileInputStream completed_img=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\completed.jpg");  
		    Image c_image = new Image(completed_img);  
		    ImageView c_img=new ImageView(c_image);
		    Button c_icon=new Button("",c_img);
		    c_icon.setWrapText(true);
			c_img.setFitHeight(250);
			c_img.setFitWidth(200);
			c_icon.setPadding(new Insets(0,0,0,0));
			recent.getChildren().add(c_icon);	
			recent.setMargin(c_icon, new Insets(0, 50, 0, 50));
			
			
//			Rectangle rec = new Rectangle();
//	        rec.setWidth(200);
//	        rec.setHeight(250);
//	        rec.setFill(Color.WHITE);
//	        recent.getChildren().add(rec);
//	        recent.setMargin(rec, new Insets(0, 50, 0, 50));
		}
		bot.getChildren().addAll(completed, recent);
		bot.setMargin(completed, new Insets(15, 30, 25, 30));
		ScrollPane botscrollPane = new ScrollPane(bot);
		botscrollPane.setMinHeight(335);
		botscrollPane.setMaxHeight(335);
		botscrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		
		
		//--integrating the projects and recently completed projects..
		
		VBox windowfull=new VBox();
		windowfull.setMinWidth(1595);
		windowfull.setPadding(new Insets(10, 10, 10, 10));
		windowfull.getChildren().addAll(mainscrollPane, botscrollPane);
		
		GridPane main=new GridPane();
		main.addRow(0, navbar, windowfull);
		

		Scene scene= new Scene(main,1960,990);
		return(scene);
		
		
	}
}
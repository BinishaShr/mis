//*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// *


import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import javafx.application.Application;  
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;  
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;  
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;  
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;  
import javax.swing.JOptionPane;
  
public class AddProject{  
  
     
    public Scene getScene(Stage primaryStage) throws Exception {

        //--sidebar
		
		VBox navbar=new VBox();
		navbar.setPadding(new Insets(10, 10, 10, 10));
//		navbar.setMinWidth(30);
		navbar.setMinHeight(1000);
		navbar.setBorder(new Border(new BorderStroke(Color.CADETBLUE, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		navbar.setSpacing(20);
		
		//navbar items
		
		//for image icon
		FileInputStream input=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");  
	    Image image = new Image(input);  
	    ImageView img=new ImageView(image);
	    Button icon=new Button("",img);
	    icon.setWrapText(true);
		img.setFitHeight(275);
		img.setFitWidth(258);
		icon.setDisable(true);
		icon.setPadding(new Insets(20,20,20,20));
		navbar.setMargin(icon, new Insets(0, 0, 50, 0));
	    
		Button home=new Button("Home");
		home.setMinWidth(300);
		home.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MainPage mainpage=new MainPage();
				try {
					Scene scene= mainpage.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					primaryStage.setMaximized(true);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button projects=new Button("All projects");
		projects.setMinWidth(300);
		projects.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				ProjectList projectlist=new ProjectList();
				try {
					Scene scene= projectlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button members=new Button("Project members");
		members.setMinWidth(300);
		members.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MemberList memberlist=new MemberList();
				try {
					Scene scene= memberlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button settings=new Button("Settings");
		settings.setMinWidth(300);
		
		navbar.getChildren().addAll(icon, home, projects, members, settings);
		
		//--sidebar

        
        Label l2=new Label("Project Name:");
        TextField t2=new TextField();
        t2.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");
        
        Label l6=new Label("Project Status:");
        TextField t6=new TextField();
        t6.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
       
        Label l3=new Label("Ending Date:");
        DatePicker t3=new DatePicker();
       // t3.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14"); 
        
        Label l4=new Label("Starting Date:");
        
       DatePicker t4=new DatePicker();
       // t4.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");
        
        Label l5=new Label("Members Id:");
        TextField t5=new TextField();
        t5.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        
        
        Button add=new Button ("Add Project");    
        add.setStyle("-fx-background-color:'#3c5a7a'; -fx-font-family:courier_new; -fx-text-fill:white;-fx-font-size:14");  
              add.setOnAction(new EventHandler<ActionEvent>() {
             
      private Connection con;

     @FXML
    private Statement stmt;

      //@FXML
    //private ResultSet rs;
            public void handle(ActionEvent event){
                try{
            		
                Class.forName("com.mysql.jdbc.Driver");
                } catch(ClassNotFoundException e) {
                    JOptionPane.showMessageDialog(null,"Unable to register class "+e.getMessage());
                }

            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost/project","root","");
                  stmt=con.createStatement();  
                }catch (SQLException e){
                    JOptionPane.showMessageDialog(null,"Unable to connect to database "+e.getMessage());
                }


                try {
                String sql;
                sql="insert into projects(project_name,project_status,due_date,starting_date,member_id)values ("
                 
                  +"'"+t2.getText()+"',"
                  
                  +"'"+t6.getText()+"',"
                  +"'"+t3.getValue()+"',"
                  +"'"+t4.getValue()+"',"
                  +"'"+t5.getText()+"');";
                        

                int result=stmt.executeUpdate(sql);

                JOptionPane.showMessageDialog(null,"Add Project ");
                } 
                catch (Exception e) {
                JOptionPane.showMessageDialog(null,"Unable to insert record "+e.getMessage());
                }


                }  });


//        Label l5=new Label("Status");
//        TextField t5=new TextField();
        GridPane root = new GridPane();   
        
        root.setStyle("-fx-background-color:Wheat; -fx-alignment:center; -fx-background-radius: 80"); 
    //-fx-alignment:center;  
    //-fx-background-radius: 100");  

        root.setHgap(8);  
        root.setVgap(15); 
       // root.addRow(0,l1,t1);  
         root.addRow(1,l2,t2);
          root.addRow(2,l3,t3);
          root.addRow(3,l6,t6);
          root.addRow(4,l4,t4);
          root.addRow(5,l5,t5);
          root.addRow(6,add);
          root.setMinWidth(1600);
          
          GridPane main=new GridPane();
		main.addRow(0, navbar, root);
          Scene scene=new Scene(main,1960,990);
          return(scene);
    } 
}

//*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// *


import javafx.application.Application;  
import javafx.scene.Scene;  
import javafx.scene.control.Button;
import javafx.scene.control.Label;  
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;  
import javafx.stage.Stage;  
  
public class Projectmanager extends Application {  
  
    @Override  
    public void start(Stage primaryStage) throws Exception {  
        // TODO Auto-generated method stub  
        Label l1=new Label("Project Name:");
        TextField t1=new TextField();
        t1.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        Label l2=new Label("Project ID:");
        TextField t2=new TextField();
        t2.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        Label l3=new Label("Ending Date:");
        TextField t3=new TextField();
        t3.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        Label l4=new Label("Starting Date:");
        
        TextField t4=new TextField();
        t4.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14"); 
        Label l5=new Label("Members Involved");
        TextField t5=new TextField();
        t5.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        
        
        Button add=new Button ("Add Project");    
        add.setStyle("-fx-background-color:'#3c5a7a'; -fx-font-family:courier_new; -fx-text-fill:white;-fx-font-size:14");  
//        Label l5=new Label("Status");
//        TextField t5=new TextField();
        GridPane root = new GridPane();   
        Scene scene=new Scene(root,300,300);
        root.setStyle("-fx-background-color:Wheat; -fx-alignment:center; -fx-background-radius: 80"); 
    //-fx-alignment:center;  
    //-fx-background-radius: 100");  

        root.setHgap(8);  
        root.setVgap(15); 
        root.addRow(0,l1,t1);  
         root.addRow(1,l2,t2);
          root.addRow(2,l3,t3);
          root.addRow(3,l4,t4);
          root.addRow(4,l5,t5);
          root.addRow(5,add);
          //root.addRow(4,l5,t5);
//        root.getChildren().add(t1);
         // root.getStylesheets().add("Project.css"); 
        primaryStage.setScene(scene);  
        primaryStage.setTitle("Add New Project");  
        primaryStage.show();  
          
    }  
    public static void main(String[] args) {  
        launch(args);  
    }  
}

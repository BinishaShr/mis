/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author kabita
 */
public class MemberForm{
     private Connection con;
     private Statement stmt;
     String member_name;
     String role;
    
    
    public Scene getScene(Stage primaryStage) throws Exception {
        
         //--sidebar
		
		VBox navbar=new VBox();
		navbar.setPadding(new Insets(10, 10, 10, 10));
//		navbar.setMinWidth(30);
		navbar.setMinHeight(1000);
		navbar.setBorder(new Border(new BorderStroke(Color.CADETBLUE, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		navbar.setSpacing(20);
		
		//navbar items
		
		//for image icon
		FileInputStream input=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");  
	    Image image = new Image(input);  
	    ImageView img=new ImageView(image);
	    Button icon=new Button("",img);
	    icon.setWrapText(true);
		img.setFitHeight(275);
		img.setFitWidth(258);
		icon.setDisable(true);
		icon.setPadding(new Insets(20,20,20,20));
		navbar.setMargin(icon, new Insets(0, 0, 50, 0));
	    
		Button home=new Button("Home");
		home.setMinWidth(300);
		home.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MainPage mainpage=new MainPage();
				try {
					Scene scene= mainpage.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					primaryStage.setMaximized(true);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button projects=new Button("All projects");
		projects.setMinWidth(300);
		projects.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				ProjectList projectlist=new ProjectList();
				try {
					Scene scene= projectlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button members=new Button("Project members");
		members.setMinWidth(300);
		members.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MemberList memberlist=new MemberList();
				try {
					Scene scene= memberlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button settings=new Button("Settings");
		settings.setMinWidth(300);
		
		navbar.getChildren().addAll(icon, home, projects, members, settings);
		
		//--sidebar

        
        
        Label l1=new Label("Member Name:");
        TextField t1=new TextField();
        t1.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        
        Label l2=new Label(" Role:");
        TextField t2=new TextField();
        t2.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14"); 
        
         
        
        
        Button btn = new Button();
        btn.setStyle("-fx-background-color:'#3c5a7a'; -fx-font-family:courier_new; -fx-text-fill:white;-fx-font-size:14");  
        btn.setText("Add");
        btn.setOnAction(new EventHandler<ActionEvent>() {
             
     

      //@FXML
    //private ResultSet rs;
            public void handle(ActionEvent event){
                member_name=t1.getText();
                role=t2.getText();
                insert();
            }});
        GridPane root = new GridPane();
        root.setHgap(8);  
        root.setVgap(15);
        root.addRow(0,l1,t1);
        root.addRow(1,l2,t2);
        root.addRow(3,btn);
        root.setMinWidth(1600);
          
          GridPane main=new GridPane();
		main.addRow(0, navbar, root);
          Scene scene=new Scene(main,1960,990);
        
       
        root.setStyle("-fx-background-color:Wheat; -fx-alignment:center; -fx-background-radius: 80"); 
       // scene.getStylesheets().add(getClass().getClassLoader().getResource("Project.css").toExternalForm());

        return(scene);
    }

    private void insert(){
        try{
            	Class.forName("com.mysql.jdbc.Driver");  
                	  
                } catch(ClassNotFoundException e) {
                    JOptionPane.showMessageDialog(null,"Unable to register class "+e.getMessage());
                }

                try {
                    con=DriverManager.getConnection("jdbc:mysql://localhost:3306/project","root","");
                    stmt=con.createStatement();  
                }catch (SQLException e){
                    JOptionPane.showMessageDialog(null,"Unable to connect to database "+e.getMessage());
                }


                try {
                String sql;
                sql="insert into members(member_name,role) values ("
                  +"'"+member_name+"',"
                  +"'"+role+"')";

                int result=stmt.executeUpdate(sql);

                JOptionPane.showMessageDialog(null,"record saved sucessfully ");
                } 
                catch (Exception e) {
                JOptionPane.showMessageDialog(null,"Unable to insert record "+e.getMessage());
                }
    }
}

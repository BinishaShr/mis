import java.io.FileInputStream;

import DataTypes.Members;
import DatabaseHandling.MemberFetch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class MemberList {
	public Scene getScene(Stage primaryStage) throws Exception{
		
		//--sidebar
		
		VBox navbar=new VBox();
		navbar.setPadding(new Insets(10, 10, 10, 10));
//		navbar.setMinWidth(30);
		navbar.setMinHeight(1000);
		navbar.setBorder(new Border(new BorderStroke(Color.CADETBLUE, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
		navbar.setSpacing(20);
		
		//navbar items
		
		//for image icon
		FileInputStream input=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");  
	    Image image = new Image(input);  
	    ImageView img=new ImageView(image);
	    Button icon=new Button("",img);
	    icon.setWrapText(true);
		img.setFitHeight(275);
		img.setFitWidth(258);
		icon.setDisable(true);
		icon.setPadding(new Insets(20,20,20,20));
		navbar.setMargin(icon, new Insets(0, 0, 50, 0));
	    
		Button home=new Button("Home");
		home.setMinWidth(300);
		home.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MainPage mainpage=new MainPage();
				try {
					Scene scene= mainpage.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					primaryStage.setMaximized(true);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button projects=new Button("All projects");
		projects.setMinWidth(300);
		projects.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				ProjectList projectlist=new ProjectList();
				try {
					Scene scene= projectlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button members=new Button("Project members");
		members.setMinWidth(300);
		members.setOnAction(new EventHandler <ActionEvent>(){
			public void handle(ActionEvent event) {
				MemberList memberlist=new MemberList();
				try {
					Scene scene= memberlist.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
			}
		});
		
		Button settings=new Button("Settings");
		settings.setMinWidth(300);
		
		navbar.getChildren().addAll(icon, home, projects, members, settings);
		
		//--sidebar
		
		FlowPane mainWin=new FlowPane();
		mainWin.setMinWidth(1595);
		
		
		VBox vbox=new VBox();
		Button add=new Button("Add Member");
                add.setOnAction(new EventHandler<ActionEvent>(){
                   public void handle(ActionEvent event){
                        MemberForm memberform=new MemberForm();
                        try{
                            Scene scene= memberform.getScene(primaryStage);
                            primaryStage.setTitle("My Project Manager");
                            primaryStage.setScene(scene);
                            primaryStage.setMaximized(true);
                        }
                        catch(Exception e){
                            System.out.println(e);
                        }
                   } 
                });
		vbox.setMargin(add, new Insets(20, 0 , 0 , 50));
                MemberFetch m=new MemberFetch();
                Members mem[]=m.getMembers();
                int count=m.getCount();
		for(int i=0; i<count; i++) {
			Text text = new Text(mem[i].name);
                        int id=mem[i].id;
			FileInputStream inputM=new FileInputStream("D:\\JAVA\\ProjectManager\\src\\Images\\icon.jpg");
			Image imageM=new Image(inputM);
			ImageView imgM=new ImageView(imageM);
			Label a_icon=new Label("",imgM);
			a_icon.setWrapText(true);
			imgM.setFitHeight(250);
			imgM.setFitWidth(200);
			a_icon.setPadding(new Insets(0,0,0,0));
			VBox member=new VBox();
			member.getChildren().addAll(a_icon, text);
			mainWin.getChildren().add(member);
                        mainWin.setMargin(member, new Insets(50, 50, 50, 50));
                        
                        a_icon.setOnMouseClicked(new EventHandler<MouseEvent>() {
			
			public void handle(MouseEvent event) {
				MemberDetails memberdetails=new MemberDetails(id);
				try {
					Scene scene= memberdetails.getScene(primaryStage);
					primaryStage.setTitle("My Project Manager");
					primaryStage.setScene(scene);
					
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
                                
			}
		});
		}
		vbox.getChildren().addAll(add, mainWin);
		
		ScrollPane mainscrollPane = new ScrollPane(vbox);
		mainscrollPane.setMinWidth(1600);
		mainscrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		
		HBox window=new HBox();
        window.getChildren().addAll(navbar, mainscrollPane);
        Scene scene=new Scene(window,1960,990);
		return(scene);
	}
}

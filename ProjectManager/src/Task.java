/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author kabita
 */
public class Task extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Label l1=new Label("Task Name:");
        TextField t1=new TextField();
         t1.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        
        Label l2=new Label("Task Detail:");
        TextField t2=new TextField();
        t2.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
         
        Label l3=new Label("Project ID:");
        TextField t3=new TextField();
        t3.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
        
        Label l4=new Label("Project Status:");
        TextField t4=new TextField();
        t4.setStyle("-fx-background-color:'#a8cef7'; -fx-text-fill:Black; -fx-font-size:14");  
    
        Button btn = new Button();
        btn.setText("Add");
        btn.setStyle("-fx-background-color:'#3c5a7a'; -fx-font-family:courier_new; -fx-text-fill:white;-fx-font-size:14");  
        
        
         btn.setOnAction(new EventHandler<ActionEvent>() {
             
     private Connection con;
    private Statement stmt;

      //@FXML
    //private ResultSet rs;
            public void handle(ActionEvent event){
                try{
            		
                Class.forName("com.mysql.jdbc.Driver");
            } catch(ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null,"Unable to register class "+e.getMessage());
            }

            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost/project","root","");
                  stmt=con.createStatement();  
                }catch (SQLException e){
                    JOptionPane.showMessageDialog(null,"Unable to connect to database "+e.getMessage());
                }


                try {
                String sql;
                sql="insert into tasks(task_name,task_details,task_status,project_id) values ("
                  +"'"+t1.getText()+"',"
                  +"'"+t2.getText()+"',"
                  +"'"+t4.getText()+"',"
                  +"'"+(Integer.parseInt(t3.getText()))+"')";
               

                int result=stmt.executeUpdate(sql);

                JOptionPane.showMessageDialog(null,"Task Assigned Succesfully ");
                } 
                catch (Exception e) {
                JOptionPane.showMessageDialog(null,"Unable to Assigned task  "+e.getMessage());
                }


                }});
        GridPane root = new GridPane();
        Scene scene = new Scene(root, 300, 250);
        root.setStyle("-fx-background-color:Wheat; -fx-alignment:center; -fx-background-radius: 80"); 
        
        root.setHgap(8);  
        root.setVgap(15);
        root.addRow(0,l1,t1);
        root.addRow(1,l2,t2);
        root.addRow(2,l3,t3);
        root.addRow(3,l4,t4);
        root.addRow(4,btn);
       
        
        
        
        primaryStage.setTitle("Task ");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
